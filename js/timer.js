
(function drawCanvas(){
  var canvas=document.getElementById('mycanvas');
  var ctx=canvas.getContext('2d');
  var cWidth=canvas.width;
  var cHeight=canvas.height;
  
  var countTo=1500;
  
  var min=Math.floor(countTo/60);
  var sec=countTo-(min*60);
  var counter=0;
  var angle=270;
  var inc=360/countTo; 
  var timerStarted = false;
  var timerReset = true;
  
  
  
  
  function drawScreen() {
    
    //reset dynamic arc
    if (timerReset) {
    	countTo=1500;
      min=Math.floor(countTo/60);
      sec=countTo-(min*60);
      counter=0;
      angle=270;
      inc=360/countTo;
    }
    
    //======= reset canvas
    
    ctx.fillStyle="#262626";
    ctx.fillRect(0,0,cWidth,cHeight);
    
    //========== base arc
    
    ctx.beginPath();
    ctx.strokeStyle="#151515";
    ctx.lineWidth=14;
    ctx.arc(cWidth/2,cHeight/2,100,(Math.PI/180)*0,(Math.PI/180)*360,false);
    ctx.stroke();
    ctx.closePath();
    
    //========== dynamic arc
    
    ctx.beginPath();
    ctx.strokeStyle="#df8209";
    ctx.lineWidth=14;
    ctx.arc(cWidth/2,cHeight/2,100,(Math.PI/180)*270,(Math.PI/180)*angle,false);
    ctx.stroke();
    ctx.closePath();
    
    //====== Labels
    
    var textColor='#cf7808';
    var textSize="12";
    var fontFace="helvetica, arial, sans-serif";
    
    ctx.fillStyle=textColor;
    ctx.font=textSize+"px "+fontFace;
    ctx.fillText('MIN',cWidth/2-46,cHeight/2-15);
    ctx.fillText('SEK',cWidth/2+25,cHeight/2-15);
    
    //====== Values
    
    ctx.fillStyle='#f6a53b';
    
    if (min>999) {
      ctx.font='60px '+fontFace;
      ctx.fillText('9' ,cWidth/2-55,cHeight/2+35);
   
    }
    else {
      ctx.font='50px '+fontFace;
      ctx.fillText(min ,cWidth/2-60,cHeight/2+35);
    }
    
    ctx.font='50px '+fontFace;
    if (sec<10) {
      ctx.fillText('0'+sec,cWidth/2+10,cHeight/2+35);
    } 
    else {
      ctx.fillText(sec,cWidth/2+10,cHeight/2+35);
    }
    
    
    if (sec<=0 && counter<countTo) {
      angle+=inc;
      counter++;
      min--;
      sec=59; 
    } else
      if (counter>=countTo) {
        sec=0;
        min=0;
      } else {
        angle+=inc;
        counter++;
        sec--;
      }
  }
  drawScreen(); 
  
  document.getElementById("start").addEventListener("click", 
    start);
  
  document.getElementById("reset").addEventListener("click", 
    reset);
  
   function start() {
      if (!timerStarted) {
        intervalId = setInterval(drawScreen,1000);
        timerStarted = true;
        timerReset = false;
      } else {
        timerStarted = false;
        clearInterval(intervalId);
      }
      console.log("timerReset is " + timerReset + "\n" + "timerStarted is " + timerStarted);
   }
  
  function reset() {
    if (timerStarted) {
      clearInterval(intervalId);
      timerStarted = false;
    }
    if (!timerReset) {
	    timerReset = true;
	    drawScreen();
    }
    console.log("timerReset is " + timerReset + "\n" + "timerStarted is " + timerStarted);
  }
  
  
  
})();

